# Commit guidelines

1. [Separate subject from body with a blank line](https://chris.beams.io/posts/git-commit/#separate)
1. [Limit the subject line to 50 characters](https://chris.beams.io/posts/git-commit/#limit-50)
1. [Capitalize the subject line](https://chris.beams.io/posts/git-commit/#capitalize)
1. [Do not end the subject line with a period](https://chris.beams.io/posts/git-commit/#end)
1. [Use the imperative mood in the subject line](https://chris.beams.io/posts/git-commit/#imperative)
1. [Wrap the body at 72 characters](https://chris.beams.io/posts/git-commit/#wrap-72)
1. [Use the body to explain what and why vs. how](https://chris.beams.io/posts/git-commit/#why-not-how)
1. [Start with an issue id](#start-with-an-issue-id)
1. [One issue per branch](#one-issue-per-branch)
1. [Issue ID is not enough](#issue-id-is-not-enough)

Following guidelines helps to keep consistency. When everyone follows the same guidelines, everyone knows what to expect from a commit message.

Chris Beams presented rules one to seven. We don't have to follow them, but these guidelines are already standard. They are deep enough into the community to be a part of default VSCode (and partly JetBrains) settings.

So if a team decides to ignore them, it's okay. But if a team decides to follow these rules, then team members can collaborate with other teams, companies, and open-source communities in a more efficient way.

Rules 8 and 9 are an extension of an original workflow.


## Start with an issue id

Describing what commit does is necessary and essential, but not enough. It's important to know what a developer wanted to do. What was the reason for these changes? Every commit has a purpose and most likely has siblings within a PR.

A bug can be caused by changes in a commit you review. Or with a commit that partially overwrites it. In both cases, you need to know a reason why these changes appear. Having a link to an issue helps to find a corresponding PR with some technical discussions.


## One issue per branch

Every commit has a parent PR. And every PR has a parent issue.
It's crucial to know a reason for changes (See rule "Start with an issue ID"). If a developer adds more than one issue per commit, it becomes unclear to understand which issue caused changes.

If you are going to implement some refactoring (that has a separate issue) in the scope of a feature branch, mention it in JIRA. Update a description or add a link. If you are performing multiple tasks as one PR, update the JIRA issue to show it explicitly.


## Issue ID is not enough

An issue tracker is responsible for acceptance criteria. A tracker describes what a user should get. But it is not responsible for a way to implement it.

It is entirely on a developer's responsibility. Describing an approach of the solution helps with an investigation. Imagine that you are trying to fix a bug that comes from 6-months old commit made by a person that doesn't work at the company anymore.

Additionally, descriptive commit messages help to reduce the code review time by describing tricky code before reviewers ask about it.
