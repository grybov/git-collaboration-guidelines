# git-collaboration-guidelines

A set of rules that describe the collaboration of developers

## Content
  - [Git commit guidelines](./commit.md)

## TODO:
  - PR collaboration
  - prepare-commit-msg hook
  - squashing / force-pushing / rebase vs merge
